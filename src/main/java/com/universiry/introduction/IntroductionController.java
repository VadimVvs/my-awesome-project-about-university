package com.universiry.introduction;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.universiry.exeptions.FacultyHaveNoPlacesExeption;
import com.universiry.model.dto.Introduction.IntroductionDTOGet;
import com.universiry.model.dto.Introduction.IntroductionDTOSaveRequest;
import com.universiry.model.dto.Introduction.IntroductionServices;
import com.universiry.model.dto.university.UniversityServices;

@RestController
@RequestMapping("/main")
public class IntroductionController {
	@Autowired
	private IntroductionServices introductionServices;

	@GetMapping("/show/all")
	@ResponseBody
	public ResponseEntity<List<IntroductionDTOGet>> findAllIntroduction() {

		return ResponseEntity.status(HttpStatus.OK).body(introductionServices.findAll());
	}

	@GetMapping("/find/{id}")
	public ResponseEntity<IntroductionDTOGet> findById(@PathVariable Long id) {

		return ResponseEntity.ok().body(introductionServices.findById(id));
	}

	@PostMapping("/new/student")
	public ResponseEntity<IntroductionDTOGet> newStudent(@RequestBody IntroductionDTOSaveRequest introductionDTO)
			throws FacultyHaveNoPlacesExeption {
		return ResponseEntity.ok().body(introductionServices.save(introductionDTO));
	}

}
