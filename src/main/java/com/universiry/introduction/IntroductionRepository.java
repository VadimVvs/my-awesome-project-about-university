package com.universiry.introduction;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;



@Component
public interface IntroductionRepository extends JpaRepository<Introduction, Long>{

}
