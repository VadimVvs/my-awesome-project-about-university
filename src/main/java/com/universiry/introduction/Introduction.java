package com.universiry.introduction;

import java.util.Objects;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Where;

import com.universiry.student.Student;
import com.universiry.university.University;

@Entity
//@Where(clause = "status=0") // true if status = "THERE_ARE_EMPTY_PLACES"
public class Introduction {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Enumerated(EnumType.ORDINAL)
	private PlaceStatus status;

	@ManyToOne
	private Student students;
	@ManyToOne
	private University university;

	public Introduction() {
	}
	
	public Introduction(Long id, PlaceStatus status, Student students, University university) {
		super();
		this.id = id;
		this.status = status;
		this.students = students;
		this.university = university;
	}

	public Introduction(Student students, University university) {
		this.students = students;
		this.university = university;
		status = PlaceStatus.THERE_ARE_EMPTY_PLACES;
	}

	public Introduction(PlaceStatus status, Student stydents, University university) {
		this.status = status;
		this.students = stydents;
		this.university = university;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PlaceStatus getStatus() {
		return status;
	}

	public void setStatus(PlaceStatus status) {
		this.status = status;
	}

	public Student getStudent() {
		return students;
	}

	public void setStudent(Student students) {
		this.students = students;
	}



	public University getUniversity() {
		return university;
	}

	public void setUniversity(University university) {
		this.university = university;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

//id -> introductionId	    
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null || getClass() != obj.getClass())
			return false;
		Introduction introduction = (Introduction) obj;
		return id.equals(introduction.id);
	}

}
