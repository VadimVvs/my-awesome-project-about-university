package com.universiry.model.dto.university;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.universiry.introduction.Introduction;
import com.universiry.introduction.PlaceStatus;
import com.universiry.model.dto.student.StudentDTO;
import com.universiry.student.Student;
import com.universiry.university.University;
import com.universiry.university.UniversityRepository;



@Service
public class UniversityServices {

	
	@Autowired
	UniversityRepository universityRepository;
	
	
		public UniversityDTO save(UniversityDTO universityDTO) {
			University university = new University();
			university.setFacultName(universityDTO.getFacultName());
			university.setPlaces(universityDTO.getPlaces());
			university.setId(universityDTO.getId());
			return universityDTO;
		}		
		
		public UniversityDTO getById(Long id) {
			return universityRepository.findById(id)
					.map(UniversityDTO::new)
					.orElse(null); 
		}
		
		public List<UniversityDTO> findAll() {
			return universityRepository.findAll()
					.stream()
					.map(UniversityDTO::new)
					.collect(Collectors.toList());
				 
		}
		
		
		public void delete(Long id) {
			universityRepository.deleteById(id);
		}
		
		public boolean HaveAPlace(University facultet) {
	        
	        List<Introduction> introductions  = facultet.getIntroduction();
	        for(Introduction introduction : introductions) {
	            if(introduction.getStatus() == PlaceStatus.THERE_ARE_EMPTY_PLACES)
	                return true;
	        }
	        return false ;
	    }

	public UniversityDTO update(UniversityDTO universityDTO, Long id) {
		return universityRepository.findById(id)
				.map( university -> {
					university.setFacultName(universityDTO.getFacultName());
					university.setPlaces(universityDTO.getPlaces());
					universityDTO.setId(universityDTO.getId());
					return new UniversityDTO(universityRepository.save(university));
				})
				.orElseThrow(null);
	}


		 

	    public List<StudentDTO> getStudentsByTheirFaculty(Long id) {
	    	University university =universityRepository.findById(id).orElse(null);
	    	return university.getIntroduction().stream()
	                .map(introduction -> new StudentDTO(introduction.getStudent()))
	                .distinct()
	                .collect(Collectors.toList());
	    }

}
