package com.universiry.model.dto.university;

import java.util.Collection;

import com.universiry.model.dto.Introduction.IntroductionDTOGet;
import com.universiry.university.University;

public class UniversityDTO {

	private Long id;

	private String facultName;

	private int places;

	public UniversityDTO() {
	}

	public UniversityDTO(Long id, String facultName, int places) {

		this.id = id;
		this.facultName = facultName;
		this.places = places;
	}

	public UniversityDTO(University university) {

		this.id = university.getId();
		this.facultName = university.getFacultName();
		this.places = university.getPlaces();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFacultName() {
		return facultName;
	}

	public void setFacultName(String facultName) {
		this.facultName = facultName;
	}

	public int getPlaces() {
		return places;
	}

	public void setPlaces(int places) {
		this.places = places;
	}

}
