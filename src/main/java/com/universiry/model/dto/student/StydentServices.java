package com.universiry.model.dto.student;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.universiry.introduction.Introduction;
import com.universiry.introduction.IntroductionRepository;
import com.universiry.introduction.PlaceStatus;
import com.universiry.model.dto.university.UniversityDTO;
import com.universiry.student.Student;
import com.universiry.student.StudentRepository;
import com.universiry.university.University;

@Service
public class StydentServices {

	@Autowired
	StudentRepository stydentRepository;

	public StudentDTO save(StudentDTO studentDTO) {
		Student student = new Student();
		student.setFullName(studentDTO.getFullName());

		return studentDTO;
	}

	public StudentDTO findOne(Long id) {
		return stydentRepository.findById(id).map(StudentDTO::new).orElseThrow(null);
	}

	public List<StudentDTO> findAll() {
		return stydentRepository.findAll().stream().map(StudentDTO::new).collect(Collectors.toList());
	}

	public List<StudentDTO> getUniversityByStydent(Long id) {
		Student student = stydentRepository.findById(id).orElseThrow(null);
		return student.getIntroduction().stream().map(introduction -> new StudentDTO(introduction.getStudent()))
				.distinct().collect(Collectors.toList());
	}

	public void deleteById(Long id) {
		stydentRepository.deleteById(id);

	}

}
