package com.universiry.model.dto.student;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.universiry.student.Student;

@Component
@Profile("!test")
public class StudentDTO {
	
	private Long id;
	
	private String FullName;
	
	
	public StudentDTO () {}
	public StudentDTO(Long id, String FullName) {
	
		this.id = id;
		this.FullName = FullName;
	}
	public StudentDTO(Student student) {
		this.id = student.getId();
		this.FullName = student.getFullName();
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFullName() {
		return FullName;
	}
	public void setFullName(String FullName) {
		this.FullName = FullName;
	}
	

}
