package com.universiry.model.dto.Introduction;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.universiry.exeptions.FacultyHaveNoPlacesExeption;
import com.universiry.introduction.Introduction;
import com.universiry.introduction.IntroductionRepository;
import com.universiry.introduction.PlaceStatus;
import com.universiry.model.dto.university.UniversityServices;
import com.universiry.student.Student;
import com.universiry.student.StudentRepository;
import com.universiry.university.University;
import com.universiry.university.UniversityRepository;

@Service
public class IntroductionServices {
	@Autowired
	private IntroductionRepository introductionRepository;

	@Autowired
	private StudentRepository studentRepository;

	@Autowired
	private UniversityRepository universityRepository;
	@Autowired
	private UniversityServices universityServices;

	public IntroductionDTOGet findById(Long id) {
		return introductionRepository.findById(id).map(IntroductionDTOGet::new).orElse(null);
	}

	public List<IntroductionDTOGet> findAll() {
		return introductionRepository.findAll().stream().map(IntroductionDTOGet::new).collect(Collectors.toList());
	}
	
	public IntroductionDTOGet save(IntroductionDTOSaveRequest introductionDTO) throws FacultyHaveNoPlacesExeption {
		Introduction introduction = new Introduction();
		
		Student student = studentRepository.findById(introductionDTO.getStudentId()).orElseThrow(null);
		University university = universityRepository.findById(introductionDTO.getUniversityId()).orElseThrow(null);
		
		introduction.setStudent(student);
		introduction.setUniversity(university);
		
		if(universityServices.HaveAPlace(introduction.getUniversity()))
			introductionRepository.save(introduction);
		else
			throw new FacultyHaveNoPlacesExeption(introduction.getUniversity().getId());
		return new IntroductionDTOGet(introduction);
		
		
	}

	public List<IntroductionDTOGet> getIntroductionByPlaceStatus(PlaceStatus placeStatus) {
		return introductionRepository.findAll().stream().map(IntroductionDTOGet::new)
				.filter(introductionDTOGet -> introductionDTOGet.getStatus() == placeStatus)
				.collect(Collectors.toList());
	}
}
