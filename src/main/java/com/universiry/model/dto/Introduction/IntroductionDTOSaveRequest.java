package com.universiry.model.dto.Introduction;

import com.universiry.introduction.Introduction;
import com.universiry.introduction.PlaceStatus;
import com.universiry.model.dto.student.StudentDTO;
import com.universiry.model.dto.university.UniversityDTO;

public class IntroductionDTOSaveRequest {


	private Long studentId;
	private Long universityId;
	private PlaceStatus status;


	public IntroductionDTOSaveRequest() {
		this.status = PlaceStatus.THERE_ARE_EMPTY_PLACES;
	}
	
	public IntroductionDTOSaveRequest(Long studentId, Long universityId, PlaceStatus status) {
		super();
		this.studentId = studentId;
		this.universityId = universityId;
		this.status = status;
	}

	public Long getStudentId() {
		return studentId;
	}


	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}


	public Long getUniversityId() {
		return universityId;
	}


	public void setUniversityId(Long universityId) {
		this.universityId = universityId;
	}


	public PlaceStatus getStatus() {
		return status;
	}


	public void setStatus(PlaceStatus status) {
		this.status = status;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((status == null) ? 0 : status.hashCode());
		result = prime * result + ((studentId == null) ? 0 : studentId.hashCode());
		result = prime * result + ((universityId == null) ? 0 : universityId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		IntroductionDTOSaveRequest other = (IntroductionDTOSaveRequest) obj;
		if (status != other.status)
			return false;
		if (studentId == null) {
			if (other.studentId != null)
				return false;
		} else if (!studentId.equals(other.studentId))
			return false;
		if (universityId == null) {
			if (other.universityId != null)
				return false;
		} else if (!universityId.equals(other.universityId))
			return false;
		return true;
	}


	





	


}
