package com.universiry.model.dto.Introduction;

import com.universiry.introduction.Introduction;
import com.universiry.introduction.PlaceStatus;
import com.universiry.model.dto.student.StudentDTO;
import com.universiry.model.dto.university.UniversityDTO;
import com.universiry.student.Student;
import com.universiry.university.University;

public class IntroductionDTOGet {

	private Long id;
	private PlaceStatus status;
	private StudentDTO studentDTO;
	private UniversityDTO universituDTO;

	public IntroductionDTOGet() {}

	public IntroductionDTOGet(Introduction introduction) {

		id = introduction.getId();
		status = introduction.getStatus();
		studentDTO = new StudentDTO(introduction.getStudent());
		universituDTO = new UniversityDTO(introduction.getUniversity());
	}

	public IntroductionDTOGet(StudentDTO student, UniversityDTO university, PlaceStatus placeStatus) {

		this.status = placeStatus;
		this.studentDTO = student;
		this.universituDTO = university;

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public PlaceStatus getStatus() {
		return status;
	}

	public void setStatus(PlaceStatus status) {
		this.status = status;
	}

	public StudentDTO getStudentDTO() {
		return studentDTO;
	}

	public UniversityDTO getUniversituDTO() {
		return universituDTO;
	}


}
