package com.universiry.university;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;



@Component
public interface UniversityRepository extends JpaRepository<University, Long>{

}
