package com.universiry.university;

import java.util.List;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.universiry.exeptions.FacultyHaveNoPlacesExeption;
import com.universiry.introduction.Introduction;;


@Entity
@Table (name="University")
@EntityListeners(AuditingEntityListener.class)
public class University {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "Facult_Id")
	private  Long id;
	@NotBlank
	@Column(name = "Facult_name")
	private String facultName;
	//@NotBlank
	@Column(name = "Places")
	private int places ;

	
	@OneToMany(mappedBy = "university")
    @JsonIgnore
    private List<Introduction> introduction;
	
	
	
	

	//------------------Getters and Setters---------------------
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFacultName() {
		return facultName;
	}
	public void setFacultName(String facultName) {
		this.facultName = facultName;
	}
	public int getPlaces() {
		return places;
	}
	public void setPlaces(int places) {
		this.places = places;
	}
	 public List<Introduction> getIntroduction() {
	        return this.introduction;
	    }
	
	
	//---------constructor-------------
	
	public University () {}

	public University(Long id, @NotBlank String facultName, int places) {
		super();
		this.id = id;
		this.facultName = facultName;
		this.places = places;
	}
	public University( @NotBlank String facultName, @NotBlank int places) {
		super();
		this.facultName = facultName;
		this.places = places;
		
	}
	
	@Override
	public String toString() {
		return "University [id=" + id + ", facultName=" + facultName + ", places=" + places + "]";
	}
	
	

}
