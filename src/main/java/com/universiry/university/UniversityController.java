package com.universiry.university;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.universiry.introduction.Introduction;
import com.universiry.model.dto.student.StudentDTO;
import com.universiry.model.dto.university.UniversityDTO;
import com.universiry.model.dto.university.UniversityServices;
import com.universiry.student.Student;

@RestController
@RequestMapping("/university")
public class UniversityController {

	@Autowired
	UniversityServices universityServices;

	// save
	@PostMapping("/save")
	public ResponseEntity<UniversityDTO> saveUniversity(@Valid @RequestBody UniversityDTO universityDTO) {

		return ResponseEntity.status(HttpStatus.CREATED).body(universityServices.save(universityDTO));
	}

	// get all
	@GetMapping("/get")
	public ResponseEntity<List<UniversityDTO>> getAllUniversity() {
		return ResponseEntity.ok().body(universityServices.findAll());
	}

	// get by id
	@GetMapping("/get/{id}")
	public ResponseEntity<UniversityDTO> getUniversityById(@PathVariable(value = "id") Long id) {

		return ResponseEntity.ok().body(universityServices.getById(id));
	}

	// update
	@PutMapping("/update/{id}")
	public ResponseEntity<UniversityDTO> updateAdmissionsEntrant(@PathVariable(value = "id") Long id,
			@Valid @RequestBody UniversityDTO universityDTO) {

		return ResponseEntity.ok().body(universityServices.update(universityDTO, id));
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<UniversityDTO> deleteAdmissionsEntrant(@PathVariable(value = "id") Long id) {

		universityServices.delete(id);
		return ResponseEntity.ok().build();
	}

	@GetMapping("/get/{id}/students")
//	List<StudentDTO> getStudentsByTheirFaculty(@PathVariable Long id) {
//		University myFaculty = universityRepository.findById(id).orElseThrow(null);
//		return myFaculty.getIntroduction().stream().map(Introduction::getStydents).collect(Collectors.toList());
//	}
	ResponseEntity<List<StudentDTO>> getStudentsByTheirFaculty(@PathVariable Long id) {
		return ResponseEntity.ok().body(universityServices.getStudentsByTheirFaculty(id));
				
	}
}
