package com.universiry.student;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.universiry.introduction.Introduction;



@Entity
@Table (name="Stydents")
@EntityListeners(AuditingEntityListener.class)
public class Student {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name = "Stydent_Id")
  private  Long id;
	@NotBlank
	@Column(name = "Full_name")
  private String fullName;


	
	 @OneToMany(mappedBy = "students")
	 @JsonIgnore
	 private List<Introduction> introduction;
	
	
	//----------------------------------------
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public List<Introduction> getIntroduction() {
		return introduction;
	}
	//-------------------------------------------------------
	public Student(Long id, @NotBlank String fullName) {
		super();
		this.id = id;
		this.fullName = fullName;
	}
	public Student(@NotBlank String fullName) {
		this.fullName = fullName;
	}
	public Student() {}

	
}
