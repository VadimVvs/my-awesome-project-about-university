package com.universiry.student;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.universiry.model.dto.student.StudentDTO;
import com.universiry.model.dto.student.StydentServices;


@RestController
@RequestMapping("/student")
public class StudentController {

	@Autowired
	StydentServices stydentServices;

	@Autowired
	StudentRepository stydentRepository;

	@PostMapping("/save")
	@ResponseStatus(HttpStatus.CREATED)
	@ResponseBody
	public StudentDTO save(StudentDTO studentDTO) {
		Student student = new Student();
		student.setFullName(studentDTO.getFullName());

		return studentDTO;
	}

	@GetMapping("/get")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public ResponseEntity<List<StudentDTO>> getAllStydents() {
	     return ResponseEntity.status(HttpStatus.OK)
	             .body(stydentServices.findAll());
	}

	@GetMapping("/get/{id}")
	@ResponseBody
	public ResponseEntity<StudentDTO> getStudentById(@PathVariable(value = "id") Long id) {

		return ResponseEntity.status(HttpStatus.OK)
                .body(stydentServices.findOne(id));
	}

	// update
	@PutMapping("/update/{id}")
	public ResponseEntity<StudentDTO> updateStudent(@PathVariable(value = "id") Long id,
			@Valid @RequestBody StudentDTO studentDetails) {

		StudentDTO StudentDTO = stydentServices.findOne(id);
		if (StudentDTO == null) {
			return ResponseEntity.notFound().build();
		}
		StudentDTO.setFullName(studentDetails.getFullName());
		StudentDTO updateStydent = stydentServices.save(StudentDTO);
		return ResponseEntity.ok().body(updateStydent);
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<StudentDTO> deleteStudent(@PathVariable(value = "id") Long id) {
		stydentServices.deleteById(id);
		return ResponseEntity.ok().build();
	}

	@GetMapping("/get/{id}/university")
	@ResponseStatus(HttpStatus.OK)
	@ResponseBody
	public ResponseEntity<List<StudentDTO>> getUniversityByStutent(@PathVariable(value = "id") Long id) {
	
		return ResponseEntity.status(HttpStatus.OK)
				.body(stydentServices.getUniversityByStydent(id));
	}

}
