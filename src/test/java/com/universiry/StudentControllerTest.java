package com.universiry;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.universiry.introduction.IntroductionController;
import com.universiry.student.StudentController;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class StudentControllerTest {
	
	 @Autowired
	    private MockMvc mockMvc;
	 
	 @Autowired
	 private StudentController studentController;
	 
	 @Test
	 public void getIdTest () throws Exception {
		 this.mockMvc.perform(get("/student/get/2"))
		 .andDo(print())
		 .andExpect(status().isOk())
		 .andExpect(content().string(containsString("{\"id\":2,\"fullName\":\"F\"}")));
	 }

}
