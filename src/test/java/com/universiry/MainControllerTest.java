package com.universiry;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.universiry.introduction.IntroductionController;


@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MainControllerTest {

	
	 @Autowired
	    private MockMvc mockMvc;
	 
	 @Autowired
	 private IntroductionController introductionController;
	 
	 @Test
	 public void mainTest() throws Exception {
		 this.mockMvc.perform(get("/main/show/all"))
		 .andDo(print())
		 .andExpect(status().isOk())
		 .andExpect(content().string(containsString("[{\"id\":1,\"status\":\"THERE_ARE_EMPTY_PLACES\",\"studentDTO\":"
		 		+ "{\"id\":1,\"fullName\":\"Osihiteo\"}"
		 		+ ",\"universituDTO\":{\"id\":3,\"facultName\":\"matan\",\"places\":1}}"
		 		+ ",{\"id\":2,\"status\":\"THERE_ARE_EMPTY_PLACES\",\"studentDTO\":{\"id\":2,\"fullName\":\"F\"}"
		 		+ ",\"universituDTO\":{\"id\":4,\"facultName\":\"mataneshte\",\"places\":1}}"
		 		+ ",{\"id\":3,\"status\":\"THERE_ARE_EMPTY_PLACES\",\"studentDTO\":{\"id\":3,\"fullName\":\"Igor\"}"
		 		+ ",\"universituDTO\":{\"id\":3,\"facultName\":\"matan\",\"places\":1}}"
		 		+ ",{\"id\":4,\"status\":\"NO_HAVE_A_PLACE\",\"studentDTO\":{\"id\":4,\"fullName\":\"Vadimeshte\"}"
		 		+ ",\"universituDTO\":{\"id\":3,\"facultName\":\"matan\",\"places\":1}}"
		 		+ ",{\"id\":5,\"status\":\"THERE_ARE_EMPTY_PLACES\",\"studentDTO\":{\"id\":1,\"fullName\":\"Osihiteo\"}"
		 		+ ",\"universituDTO\":{\"id\":4,\"facultName\":\"mataneshte\",\"places\":1}}]")));
		
	 }

	
}
